<?php
declare(strict_types=1);

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    // Implement common logic
    public function initialize()
    {
        // $di = $this->app->di;
        if ($this->di->get('request')->getServer('HTTP_ORIGIN')) {
            $origin = $this->di->get('request')->getServer('HTTP_ORIGIN');
        } else {
            $origin = '*';
        }

        $this->response->setHeader('Access-Control-Allow-Origin', $origin);
        $this->response->setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        $this->response->setHeader('Access-Control-Allow-Credentials', 'true');
        $this->response->setHeader('Access-Control-Allow-Headers', 'Content-Type, Origin, x-origin, authorization, Cache-Control, x-cypress-is-xhr-or-fetch, *');
        $this->response->setHeader('Content-Type', 'application/json');
    }
}
