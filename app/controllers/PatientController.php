<?php
declare(strict_types=1);

use Phalcon\Http\Response;

class PatientController extends ControllerBase
{

    public function helloAction(): Response
    {
        return $this->response->setJsonContent([
            'message' => 'Hello, World! Phalcon Version 5 Working Now Micro API',
            'ip' => $this->request->getClientAddress(),
            'Phalcon' => (new Phalcon\Support\Version())->get(),
            'PHP' => PHP_VERSION,
            'os' => php_uname(),
        ]);
    }

    public function indexAction(): Response
    {
        $patients = Patient::find();
        return $this->response->setJsonContent([
            "status" => [
                "code" => 200,
                "response" => "success",
                "message" => "success get data patients"
            ],
            "result" => $patients->toArray()
        ]);
    }

    public function showAction($id): Response
    {
        $patient = Patient::findFirstById($id);

        if (!$patient) {
            $this->response->setStatusCode(404, "Not Found");
            return $this->response->setJsonContent([
                "status" => [
                    "code" => 404,
                    "response" => "error",
                    "message" => "patient not found"
                ],
                "result" => []
            ]);
        } else {
            $this->response->setStatusCode(200, "OK");
            return $this->response->setJsonContent([
                "status" => [
                    "code" => 200,
                    "response" => "success",
                    "message" => "success get data patient"
                ],
                "result" => $patient
            ]);
        }
    }

    public function createAction()
    {
        $patientData = new Patient();
        $requestData = $this->request->getJsonRawBody();
        $patientData->name = $requestData->name;
        $patientData->sex = $requestData->sex;
        $patientData->religion = $requestData->religion;
        $patientData->phone = $requestData->phone;
        $patientData->address = $requestData->address;
        $patientData->nik = $requestData->nik;

        if ($patientData->save()) {
            return $this->response->setJsonContent([
                "status" => [
                    "code" => 201,
                    "response" => "success",
                    "message" => "patient created successfully"
                ],
                "result" => $patientData->toArray()
            ])->setStatusCode(201, "Created");
        } else {
            return $this->response->setJsonContent([
                "status" => [
                    "code" => 400,
                    "response" => "error",
                    "message" => "failed to create patient",
                    "errors" => $patientData->getMessages()
                ],
                "result" => []
            ])->setStatusCode(400, "Bad Request");
        }
    }


    public function updateAction($id)
    {
        $patient = Patient::findFirstById($id);

        if (!$patient) {
            return $this->response->setJsonContent([
                "status" => [
                    "code" => 404,
                    "response" => "error",
                    "message" => "patient not found"
                ],
                "result" => []
            ])->setStatusCode(404, "Not Found");
        }

        $putData = $this->request->getPut();

        $patient->assign($putData);

        if ($patient->save()) {
            return $this->response->setJsonContent([
                "status" => [
                    "code" => 200,
                    "response" => "success",
                    "message" => "patient updated successfully"
                ],
                "result" => $patient->toArray()
            ])->setStatusCode(200, "Success");
        } else {
            return $this->response->setJsonContent([
                "status" => [
                    "code" => 400,
                    "response" => "error",
                    "message" => "failed to update patient",
                    "errors" => $patient->getMessages()
                ],
                "result" => []
            ])->setStatusCode(400, "Bad Request");
        }
    }

    public function deleteAction($id): Response
    {
        $patient = Patient::findFirstById($id);

        if (!$patient) {
            return $this->response->setJsonContent([
                "status" => [
                    "code" => 404,
                    "response" => "error",
                    "message" => "patient not found"
                ],
                "result" => []
            ])->setStatusCode(404, "Not Found");
        }

        if ($patient->delete()) {
            return $this->response->setJsonContent([
                "status" => [
                    "code" => 200,
                    "response" => "success",
                    "message" => "patient deleted successfully"
                ],
                "result" => []
            ])->setStatusCode(200, "Success");
        } else {
            return $this->response->setJsonContent([
                "status" => [
                    "code" => 400,
                    "response" => "error",
                    "message" => "failed to delete patient"
                ],
                "result" => []
            ])->setStatusCode(400, "Bad Request");
        }
    }

}

