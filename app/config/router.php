<?php

use Phalcon\Mvc\Micro;

$router = $di->getRouter();

$router->addGet('/hello', [
    'controller' => 'Patient',
    'action'     => 'hello',
]);

// Patient
$router->addGet('/patient', [
    'controller' => 'Patient',
    'action'     => 'index',
]);
$router->addGet('/patient/{id}', [
    'controller' => 'Patient',
    'action' => 'show',
]);
$router->addPost('/patient', [
    'controller' => 'patient',
    'action' => 'create'
]);
$router->addPut('/patient/{id}', [
    'controller' => 'patient',
    'action' => 'update'
]);
$router->addDelete('/patient/{id}', [
    'controller' => 'patient',
    'action' => 'delete'
]);

// $router->handle($_SERVER['REQUEST_URI']); // origin

$app = new Micro($di);

$app->setService('router', $router, true);
